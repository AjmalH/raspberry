#!/usr/bin/env python3
import time
import subprocess
import requests
from board import SCL, SDA
import busio
from PIL import Image, ImageDraw, ImageFont
import adafruit_ssd1306
from time import *
import time
import datetime
import json

# Create the I2C interface.
i2c = busio.I2C(SCL, SDA)

# Create the SSD1306 OLED class from Adafruit's library
# Uncomment this following line for 128x64 display
disp = adafruit_ssd1306.SSD1306_I2C(128, 64, i2c)
# Uncomment this following line for 128x32 display
# disp = adafruit_ssd1306.SSD1306_I2C(128, 32, i2c)

# Uncomment this following line to rotate the screen (0 = default, 2 = 180°)
disp.rotation = 2

# Uncomment this following line to invert the color (Black text on white screen)
#disp.invert(1)

# Variables used to draw text
width = disp.width
height = disp.height
padding = -2
top = padding
bottom = height-padding
x = 0

# We need to create an empty image to draw text into it:
# Create blank image for drawing with 1-bit color
image = Image.new('1', (width, height))

# Get drawing object to draw on image
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image
draw.rectangle((0, 0, width, height), outline=0, fill=0)

# Set the path of the font you want to use
font_path='/usr/local/share/fonts/Menlo-Regular.ttf'
# Default font
# font_path='/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'
#font_path='/usr/local/share/fonts/FiraCode/FiraCode-Regular.ttf'

# List of all commands
cmd_hostname="hostname -I | cut -d\' \' -f1"
cmd_CPU="top -bn1 | grep load | awk '{printf \"CPU:%.2f\", $(NF-2)}'"
cmd_mem_1="free -m | awk 'NR==2{printf \"Mem: %s/%s MB  %.2f%%\", $3,$2,$3*100/$2 }'"
cmd_mem_2="free -m | awk 'NR==2{printf \"Mem:%.0f%%\", $3*100/$2 }'"
cmd_disk="df -h | awk '$NF==\"/\"{printf \"Disk: %d/%d GB  %s\", $3,$2,$5}'"
cmd_CPUtemp="vcgencmd measure_temp | cut -d ' ' -f2  | cut -d '=' -f2 | cut -d '.' -f1"
# If you have a 1-wire temperature sensor. Change 28-0416a190deff with your temp sensor address
#cmd_temp = "cat /sys/bus/w1/devices/28-0416a190deff/w1_slave | tail +2 | cut -d'=' -f2"

while True:
    # Draw a black filled box to clear the image.
    draw.rectangle((0, 0, width, height), outline=0, fill=0)
    Temp_file = open("/home/harkaige/temp_cadre", "r")
    Weather_file = open("/home/harkaige/temp_weather", "r")
    Temp = "{}°C {}°C".format(Temp_file.readline().strip(), Weather_file.readline().strip())
    Temp_file.close()
    Weather_file.close()

    CPU = subprocess.check_output(cmd_CPU, shell=True).decode("utf-8")
    MemUsage = subprocess.check_output(cmd_mem_2, shell=True).decode("utf-8")
    txt_stats = "{} {}".format(CPU, MemUsage)
    weekdays = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
    time_ = datetime.datetime.now().strftime("%H:%M:%S")
    weekday = weekdays[datetime.datetime.now().weekday()]
    date_ = weekday + " " + datetime.datetime.now().strftime("%d/%m")

    draw.text((x, top), time_, font=ImageFont.truetype(font_path, 25), fill=255)
    draw.text((x, top+23), date_, font=ImageFont.truetype(font_path, 17), fill=255)
    draw.text((x, top+23+17), Temp, font=ImageFont.truetype(font_path, 14), fill=255)
    draw.text((x, bottom-15), txt_stats, font=(ImageFont.truetype(font_path, 14)), fill=255)

    config_file = open("/var/www/html/config", "r")
    config = config_file.readline().strip()
    config_file.close()
    if(config == "0"):
        draw.rectangle((0, 0, width, height), outline=0, fill=0)

    # Update the screen with the updated image
    disp.image(image)
    disp.show()
    time.sleep(.75)